<?php
/**
 * @file
 * showcase_two_column.features.inc
 */

/**
 * Implements hook_node_info().
 */
function showcase_two_column_node_info() {
  $items = array(
    'showcase_twocol' => array(
      'name' => t('Showcase (two column)'),
      'base' => 'node_content',
      'description' => t('Used to create a two-column showcase widget, for display at the top of section landing pages.'),
      'has_title' => '1',
      'title_label' => t('Header'),
      'help' => t('Notes:
<ul>
<li>A two-column showcase can not be published until at least one item is added.</li>
<li>Each item in a two-column showcase should contain either an image or a video, but not both.</li>
</ul>
<strong>Image size info</strong>
428px by 196px'),
    ),
  );
  return $items;
}
