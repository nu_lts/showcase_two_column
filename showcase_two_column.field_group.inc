<?php
/**
 * @file
 * showcase_two_column.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function showcase_two_column_field_group_info() {
  $export = array();

  $field_group = new stdClass;
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_field_sc_twocol_item|field_collection_item|field_sc_twocol_item|form';
  $field_group->group_name = 'group_field_sc_twocol_item';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_sc_twocol_item';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Item',
    'weight' => '0',
    'children' => array(
      0 => 'field_sc_twocol_item_headline',
      1 => 'field_sc_twocol_item_image',
      2 => 'field_sc_twocol_item_link',
      3 => 'field_sc_twocol_item_text',
      4 => 'field_sc_twocol_item_type',
      5 => 'field_sc_twocol_item_video',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Item',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_field_sc_twocol_item|field_collection_item|field_sc_twocol_item|form'] = $field_group;

  return $export;
}
